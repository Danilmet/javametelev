package ru.nsu.ccfit.g1505.metelev;

import java.io.Reader;
import java.io.Writer;
import java.util.*;
import java.lang.*;


class WordProcessor {
    private Reader reader;
    private Writer writer;
    WordProcessor(Reader reader, Writer writer){
        this.reader = reader;
        this.writer = writer;
    }
    private HashMap<String, Counter> words = new HashMap<String, Counter>();
    void Analize(){
        TXTParser txtParser = new TXTParser();
        txtParser.parser(reader);
        for (int i = 0; i < txtParser.words.size(); i++){
            if (words.containsKey(txtParser.words.get(i))) {
                words.get(txtParser.words.get(i)).incr();
            }
            else words.put(txtParser.words.get(i), new Counter(1));
        }
        List<Map.Entry<String, Counter>> list = new LinkedList<Map.Entry<String, Counter>>(words.entrySet());
        ReportBuilder sort = new ReportBuilder();
        list = sort.GetOrderedStatistic(words);

        CSVReportGenerator out = new CSVReportGenerator();
        out.Output(writer, list, txtParser.words.size());
    }
}
