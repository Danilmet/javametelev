package ru.nsu.ccfit.g1505.metelev;

import java.io.Writer;
import java.util.List;
import java.util.Map;


public interface ReportGenerator {
    void Output(Writer outStream,List<Map.Entry<String, Counter>> list, int size);
}
