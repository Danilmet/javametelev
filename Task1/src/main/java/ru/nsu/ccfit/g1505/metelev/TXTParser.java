package ru.nsu.ccfit.g1505.metelev;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class TXTParser implements Parser {
    List<String> words = new ArrayList<String>();
    public void parser(Reader readStream){
        try {
            int symbol = 0;
            StringBuilder word = new StringBuilder();
            String result = "";
            while (symbol != -1) {
                symbol = readStream.read();
                if (Character.isLetterOrDigit(symbol)) {
                    word.append((char)symbol);
                }
                else {

                    result = word.toString();
                    if (result.length() != 0) {
                        words.add(result);
                        word = new StringBuilder();
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace(System.err);
        }

    }
}
