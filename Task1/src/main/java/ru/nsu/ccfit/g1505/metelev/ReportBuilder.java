package ru.nsu.ccfit.g1505.metelev;

import java.util.*;

public class ReportBuilder {
    public List<Map.Entry<String, Counter>> GetOrderedStatistic(HashMap<String, Counter> words)
    {
        List<Map.Entry<String, Counter>> list = new LinkedList<Map.Entry<String, Counter>>(words.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Counter>>()
        {
            public int compare(Map.Entry<String, Counter> o1, Map.Entry<String, Counter> o2)
            {
                return ((Integer)o2.getValue().count).compareTo(o1.getValue().count);
            }
        });
        return list;
    }
}

