package ru.nsu.ccfit.g1505.metelev;

import java.io.*;
import java.util.logging.Logger;

public class MainApp
{
    public static void main( String[] args )
    {
        Reader reader = null;
        Writer writer = null;
        try
        {
            reader = new InputStreamReader(new FileInputStream("H:\\WordProcessor.txt"), "UTF-8");
            writer = new OutputStreamWriter(new FileOutputStream("H:\\output.csv"), "UTF-8");
            WordProcessor wordProcessor = new WordProcessor(reader, writer);
            wordProcessor.Analize();

        }
        catch (IOException e)
        {
            System.err.printf("Error while reading/writing file: %s%n" + e.getLocalizedMessage());
        }
        finally
        {
            if ((null != reader) & (null != writer)) {
                try {
                    reader.close();
                    writer.close();
                }
                catch (IOException e){
                    e.printStackTrace(System.err);
                }
            }

        }
    }
}
