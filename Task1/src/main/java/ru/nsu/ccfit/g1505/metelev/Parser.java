package ru.nsu.ccfit.g1505.metelev;

import java.io.Reader;
import java.util.List;

public interface Parser {
    List<String> words = null;
    public void parser(Reader readStream);
}
