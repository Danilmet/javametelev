package ru.nsu.ccfit.g1505.metelev;

import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;



public class CSVReportGenerator implements ReportGenerator {
    private static final String CSV_DELIMITER = " ; ";
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    public void Output(Writer outStream, List<Map.Entry<String, Counter>> list, int size){
        double toAll = 0.0;
        DecimalFormat format = new DecimalFormat("#.###");
        for (Map.Entry <String, Counter> text: list)
        {
            Object word = text.getKey();
            Counter count = text.getValue();
            toAll = (double)count.count/(double)size*100.0;
            StringBuilder builder = new StringBuilder();
            builder.append(word).append(CSV_DELIMITER)
                    .append(count.count).append(CSV_DELIMITER)
                    .append(format.format(toAll)).append(LINE_SEPARATOR);
            String out = builder.toString();
            try{
                outStream.write(out);
            }
            catch (IOException e){
                System.err.println("Error while writing file: " + e.getLocalizedMessage());
            }
        }
    }
}
