package ru.nsu.ccfit.g15205.metelev.commands;

import ru.nsu.ccfit.g15205.metelev.Command;
import java.util.Map;
import java.util.Stack;
import ru.nsu.ccfit.g15205.metelev.CalcException;


public class Print implements Command{
    public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException {
        if (stack.size() == 0) {
            CalcException e = new CalcException ("Empty stack!");
            throw e;
        } else {
            System.out.println(stack.peek());
        }
    }
}
