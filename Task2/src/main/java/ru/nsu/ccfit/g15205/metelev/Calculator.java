package ru.nsu.ccfit.g15205.metelev;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

public class Calculator {
    private Scanner scanner = null;
    public Calculator(InputStream stream) {
        scanner = new Scanner(stream);
    }

    public void execute() throws IOException{
        String s;
        Stack<Double> stack = new Stack<Double>();
        Map<String, Double> map = new HashMap<String, Double>();
        CommandsFactory factory = CommandsFactory.getInstance();

        while (scanner.hasNextLine()) {
            s = scanner.nextLine();
            String[] input = s.split(" ");
            input[0] = input[0].toLowerCase();
            Command com = factory.getCommandByName(input[0]);
            if (com != null) {
                try {
                    com.exec(stack, map, input);
                } catch (CalcException e) {
                    System.out.println("Command isn't executed. " + e.getMessage());
                }
            } else {
                System.out.println("Command with name " + input[0] + " does not exist!");
            }
        }
        scanner.close();
    }


}
