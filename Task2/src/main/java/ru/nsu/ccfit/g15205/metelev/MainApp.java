package ru.nsu.ccfit.g15205.metelev;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;

public class MainApp
{
    private static final Logger log = LogManager.getLogger();
    public static void main( String[] args )
    {
        log.debug("Start working of Calculator");
        InputStream reader = null;
        if (args[0] != null){
            try {
                log.info("Using arg as filename: {}", args[0]);
                reader = new FileInputStream(args[0]);
            }
            catch (IOException e){
                log.fatal("Can't open file {} for reading", args[0]);
                System.err.println("Error while reading file: " + e.getLocalizedMessage());
            }
        }
        else if (args.length > 0) {
            log.info("Using command arguments");
            reader = System.in;
        }
        Calculator calc = new Calculator(reader);
        try{
            calc.execute();
        } catch (IOException e) {
            System.out.println("Cannot open commands");
        }
        finally {
            if (reader != null){
                try {
                    reader.close();
                }
                catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }
    }
}
