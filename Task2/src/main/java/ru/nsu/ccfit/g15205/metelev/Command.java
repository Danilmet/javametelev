package ru.nsu.ccfit.g15205.metelev;


import java.util.Map;
import java.util.Stack;

public interface Command {
    void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException;
}
