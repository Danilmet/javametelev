package ru.nsu.ccfit.g15205.metelev.commands;


import ru.nsu.ccfit.g15205.metelev.Command;
import java.util.EmptyStackException;
import java.util.Map;
import java.util.Stack;
import ru.nsu.ccfit.g15205.metelev.CalcException;

public class Pop implements Command{
    public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException {
        try {
            System.out.println(stack.pop());
        } catch (EmptyStackException e) {
            CalcException e1 = new CalcException ("Empty stack!");
            throw e1;
        }
    }
}
