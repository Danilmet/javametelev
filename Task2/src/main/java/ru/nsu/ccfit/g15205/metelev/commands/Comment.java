package ru.nsu.ccfit.g15205.metelev.commands;


import java.util.Map;
import java.util.Stack;
import ru.nsu.ccfit.g15205.metelev.Command;

public class Comment implements Command{
    public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) {
        for (int i = 1; i < argv.length - 1; i++) {
            System.out.print(argv[i] + " ");
        }
        System.out.println(argv[argv.length - 1]);
    }
}

