package ru.nsu.ccfit.g15205.metelev;


import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class CommandsFactory {
    private final Map<String, Command> map = new HashMap<String, Command>();
    private static CommandsFactory ins;

    private CommandsFactory () throws IOException {
        Properties prop = new Properties();
        InputStream iStream = CommandsFactory.class.getClassLoader().getResourceAsStream("commandsTitle.properties");
        if (iStream == null) {
            IOException e = new IOException();
            throw e;
        }
        prop.load(iStream);

        for (String key : prop.stringPropertyNames()) {
            prop.get(key);
            try {
                Class<?> com = Class.forName(prop.getProperty(key));
                map.put(key, (Command) com.newInstance());
            } catch (Exception e) {
                System.out.println("Error adding command " + key);
            }
        }
    }

    public Command getCommandByName (String name) {
        return map.get(name);
    }

    public static CommandsFactory getInstance () throws IOException {
        if (ins == null)
            ins = new CommandsFactory();
        return ins;
    }
}