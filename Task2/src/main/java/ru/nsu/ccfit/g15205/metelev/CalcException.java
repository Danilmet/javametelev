package ru.nsu.ccfit.g15205.metelev;


public class CalcException extends Exception {

    public CalcException (String message) {
        super(message);
    }

}
