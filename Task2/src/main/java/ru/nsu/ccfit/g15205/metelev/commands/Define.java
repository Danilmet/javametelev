package ru.nsu.ccfit.g15205.metelev.commands;


import ru.nsu.ccfit.g15205.metelev.Command;
import java.util.Map;
import java.util.Stack;
import ru.nsu.ccfit.g15205.metelev.CalcException;

public class Define implements Command{
    public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException{
        try {
            param.put(argv[1], Double.valueOf(argv[2]));
        } catch (NumberFormatException e) {
            CalcException e1 = new CalcException ("Second argument is not a number!");
            throw e1;
        } catch (ArrayIndexOutOfBoundsException e) {
            CalcException e2 = new CalcException ("Not enough arguments!");
            throw e2;
        }
    }
}