package ru.nsu.ccfit.g15205.metelev.commands;


import ru.nsu.ccfit.g15205.metelev.Command;
import java.util.Map;
import java.util.Stack;
import ru.nsu.ccfit.g15205.metelev.CalcException;


public class Division implements Command{
    public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException {
        if (stack.size() == 0) {
            CalcException e1 = new CalcException ("Empty stack!");
            throw e1;
        } else if (stack.size() == 1) {
            CalcException e2 = new CalcException ("Stack has only 1 element!");
            throw e2;
        } else {
            stack.push( 1 / stack.pop() * stack.pop());
        }
    }
}
